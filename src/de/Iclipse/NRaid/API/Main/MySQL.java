package de.Iclipse.NRaid.API.Main;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.sql.*;

import static de.Iclipse.NRaid.API.Data.General.instance;

/**
 * Created by Iclipse who don't like people which steal this code!!
 */
public class MySQL {

    public static File getMySQLFile() {
        return new File("plugins/" + instance.getDescription().getName(), "mysql.yml");
    }

    public static FileConfiguration getMySQLFileConfiguration() {
        return YamlConfiguration.loadConfiguration(getMySQLFile());
    }

    public static void setStandardMySQL() {
        FileConfiguration cfg = getMySQLFileConfiguration();
        cfg.options().copyDefaults(true);
        cfg.addDefault("host", "localhost");
        cfg.addDefault("database", "Api");
        cfg.addDefault("user", "mysql");
        cfg.addDefault("password", "1HM4jY18");
        cfg.addDefault("prefix", "&5" + instance.getDescription().getName() + " &3MySQL &8&7");
        try {
            cfg.save(getMySQLFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void readMySQL() {
        FileConfiguration cfg = getMySQLFileConfiguration();
        HOST = cfg.getString("host");
        DATABASE = cfg.getString("database");
        USER = cfg.getString("user");
        PASSWORD = cfg.getString("password");
        prefix = ChatColor.translateAlternateColorCodes('&', cfg.getString("prefix")) + " ";
    }


    private static String HOST;
    private static String DATABASE;
    private static String USER;
    private static String PASSWORD;
    private static String prefix;

    public static Connection conn;

    public MySQL(String database) {
        this.DATABASE = database;
        connect();
    }

    public static void connect() {
        setStandardMySQL();
        readMySQL();
        try {
            conn = DriverManager.getConnection("jdbc:mysql://" + HOST + ":3306/" + DATABASE + "?autoReconnect=false", USER, PASSWORD);
            System.out.println(prefix + "Verbunden!");
        } catch (SQLException e) {
            System.out.println(prefix + "Keine Verbindung! Fehler: " + e.getMessage());
        }
    }

    public static void close() {
        try {
            if (conn != null) {
                conn.close();
                System.out.println(prefix + "erfolgreich getrennt!");
            }

        } catch (SQLException e) {
            System.out.println(prefix + "Keine Verbindung! Fehler: " + e.getMessage());
        }
    }

    public static void update(String querry) {
        Statement st;
        try {
            st = conn.createStatement();
            st.executeUpdate(querry);
            st.close();
        } catch (SQLException e) {
            connect();
            System.err.println(e);
        }
    }

    public static ResultSet querry(String querry) {
        ResultSet rs = null;

        Statement st;
        try {
            st = conn.createStatement();
            rs = st.executeQuery(querry);
        } catch (SQLException e) {
            connect();
            System.err.println(e);
        }
        return rs;
    }
}
