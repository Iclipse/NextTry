package de.Iclipse.NRaid.API.Main;

import org.bukkit.plugin.java.JavaPlugin;

import static de.Iclipse.NRaid.API.Data.ConfigurationFiles.readConfigurationFiles;
import static de.Iclipse.NRaid.API.Data.General.instance;

/**
 * Created by Yannick who could get really angry if somebody steal his code!
 * ~Yannick on 31.12.2017 at 12:02 o´ clock
 */
public class Main extends JavaPlugin {

    @Override
    public void onEnable() {
        instance = this;
        MySQL.connect();
        readConfigurationFiles();
    }

    @Override
    public void onDisable() {

    }
}
