package de.Iclipse.NRaid.API.Data;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

import static de.Iclipse.NRaid.API.Data.General.instance;
import static de.Iclipse.NRaid.API.Main.MySQL.readMySQL;

/**
 * Created by Yannick who could get really angry if somebody steal his code!
 * ~Yannick on 31.12.2017 at 12:22 o´ clock
 */
public class ConfigurationFiles {

    public static void readConfigurationFiles() {
        readConfig();
        readMySQL();
        readPrefix();
        readLanguage();
    }


    //Config File
    public static File getConfigFile() {
        return new File("plugins" + File.pathSeparator + instance.getDescription().getName(), "config.yml");
    }

    public static FileConfiguration getConfigFileConfiguration() {
        return YamlConfiguration.loadConfiguration(getConfigFile());
    }

    public static void setStandardConfig() {
        FileConfiguration cfg = getConfigFileConfiguration();
        cfg.options().copyDefaults(true);
        try {
            cfg.save(getConfigFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void readConfig() {

    }


    //Prefix File
    public static File getPrefixFile() {
        return new File("plugins" + File.pathSeparator + instance.getDescription().getName(), "prefix.yml");
    }

    public static FileConfiguration getPrefixFileConfiguration() {
        return YamlConfiguration.loadConfiguration(getPrefixFile());
    }

    public static void setStandardPrefix() {
        FileConfiguration cfg = getPrefixFileConfiguration();
        cfg.options().copyDefaults(true);
        cfg.addDefault("prefix", "§8[§3Night§6Raid§8]§7");
        cfg.addDefault("sysprefix", "§8[§4System§8]§7");
        try {
            cfg.save(getPrefixFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void readPrefix() {

    }

    //Permissions File
    public static File getPermissionsFile() {
        return new File("plugins" + File.pathSeparator + instance.getDescription().getName(), "permissions.yml");
    }

    //Language Files

    //German
    public static File getLanguageFileGER() {
        return new File("plugins" + File.pathSeparator + "lang", "lang-GER");
    }

    public static FileConfiguration getLanguageGERFileConfiguration() {
        return YamlConfiguration.loadConfiguration(getLanguageFileGER());
    }


    //English
    public static File getLanguageFileEN() {
        return new File("plugins" + File.pathSeparator + "lang", "lang-EN");
    }

    public static FileConfiguration getLanguageENFileConfiguration() {
        return YamlConfiguration.loadConfiguration(getLanguageFileEN());
    }

    public static void readLanguage() {

    }

}
